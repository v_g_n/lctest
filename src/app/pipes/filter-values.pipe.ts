import { Pipe, PipeTransform } from '@angular/core';
import {_} from 'underscore';


@Pipe({
  name: 'filterValues'
})
export class FilterValuesPipe implements PipeTransform {

  transform(users, filterType) {
    const
      lengths = [],
      filters = _.unique(users.map(u => u[filterType]));
    for (const filter of filters) {
      const obj = {};
      obj[filterType] = filter;
      lengths.push(_.where(users, obj).length);
    }
    return {
      filterType: filterType,
      values: filters,
      lengths: lengths
    };
  }

}
