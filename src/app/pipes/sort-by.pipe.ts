import { Pipe, PipeTransform } from '@angular/core';
import {_} from 'underscore';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(value, options) {
    return options.reverse ? _.sortBy(value, options.sortedBy).reverse() : _.sortBy(value, options.sortedBy);
  }

}
