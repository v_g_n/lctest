import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {
  @Input() users;
  sortedBy = 'name';
  reverse = false;
  tableMap = ['name', 'age', 'gender', 'department', 'address'];
  constructor( ) {
  }
  ngOnInit() {

  }

  public sortBy(type) {
    if (type === 'address') { type = 'city'; }
    if (this.sortedBy === type) {
      this.reverse = !this.reverse;
    } else {
      this.sortedBy = type;
      this.reverse = false;
    }
  }
}
