import { Component } from '@angular/core';
import {UsersService} from './users.service';
import {_} from 'underscore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  users = [];
  filteredUsers = [];
  filtersList = [];
  errorText = '';
  constructor(private UsersService: UsersService) {
     this.UsersService.getUsers()
       .subscribe(data => {
         let n = 0;
         for (const item of data) {
           this.users[n] = {
             name: item.name,
             age: item.age,
             gender: item.gender,
             department: item.department,
             city: item.address.city,
             street: item.address.street
           };
           n++;
         }
         this.filteredUsers = this.users;
       },
       error => {
         this.showServerError('We have some error: ' + error.message);
       }
       );
  }
  public onFilter(filterValue) {
    const that = this;
    this.filteredUsers = this.users;
    if (_.findWhere(this.filtersList, filterValue)) {
      this.filtersList = this.filtersList.filter(function (fv) {
        return !_.isEqual(fv, filterValue);
      });
    } else {
      this.filtersList.push(filterValue);
    }
    if (this.filtersList !== undefined || this.filtersList.length > 0) {
      this.filtersList.forEach(el => this.filteredUsers = _.where(this.filteredUsers, el));
    }
  }

  public showServerError(text) {
    this.errorText = text;
  }
}
