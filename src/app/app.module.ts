import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { FilterValuesPipe } from './pipes/filter-values.pipe';
import { FilterComponent } from './filter/filter.component';
import {HttpClientModule} from '@angular/common/http';
import { SortByPipe } from './pipes/sort-by.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    FilterValuesPipe,
    FilterComponent,
    SortByPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
