import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.sass']
})
export class FilterComponent implements OnInit {
  @Input() data;
  @Output() filterValue: EventEmitter<object> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  public filterChecked(e) {
    const obj = {};
    obj[this.data.filterType] = e.target.getAttribute('data-value');
    this.filterValue.emit(obj);
  }
}
